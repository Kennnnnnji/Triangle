package delaunay;

import java.util.Iterator;
import java.util.Set;

public class Geo {
    Set<Point> vertices;

    Point get(int i) {
        Iterator<Point> it = vertices.iterator();
        int j = 0;
        Point ret = null;
        while (it.hasNext() && j <= i) {
            j++;
            ret = it.next();
        }
        return ret;
    }
}
