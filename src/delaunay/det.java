package delaunay;

public class Det {
    public static double Det(double[][] Matrix, int N)//计算n阶行列式（N=n-1）
    {
        int T0;
        int T1;
        int T2;
        double Num;
        int Cha;
        double[][] B;
        if (N > 0) {
            Cha = 0;
            B = new double[N][N];
            Num = 0;
            if (N == 1) {
                return Matrix[0][0] * Matrix[1][1] - Matrix[0][1] * Matrix[1][0];
            }
            for (T0 = 0; T0 <= N; T0++) {
                for (T1 = 1; T1 <= N; T1++) {
                    for (T2 = 0; T2 <= N - 1; T2++) {
                        if (T2 == T0) {
                            Cha = 1;

                        }
                        B[T1 - 1][T2] = Matrix[T1][T2 + Cha];
                    }
                    Cha = 0;
                }
                Num = Num + Matrix[0][T0] * Det(B, N - 1) * Math.pow((-1), T0);
            }
            return Num;
        } else if (N == 0) {
            return Matrix[0][0];
        }

        return 0;
    }
}
