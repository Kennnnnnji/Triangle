package delaunay;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class Edge extends Geo {
    int ai, bi;

    Edge(Point a, Point b) {
        vertices = new HashSet<>();
        vertices.add(a);
        vertices.add(b);
        ai = a.index;
        bi = b.index;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Edge)) {
            return false;
        }
        return vertices.equals(((Edge) obj).vertices);
    }

    void rock() {
        List<Triangle> maybeTri = new ArrayList<>();
        Edge edge = this;
        for (int i = 0; i < Main.vertices; i++) {
            if (edge.vertices.contains(Main.points[i])) {
                continue;
            }
            Triangle triangleA = new Triangle(edge.get(0), edge.get(1), Main.points[i]);
            triangleA.setNewPoint(Main.points[i]);
            Edge[] new2edges = {new Edge(get(0), Main.points[i]), new Edge(get(1), Main.points[i])};
            triangleA.setEdge(edge, new2edges);
            boolean flag = false;
            for (int j = 0; j < Main.vertices; j++) {
                if (triangleA.contains(Main.points[j])) {
                    continue;
                }
                if (triangleA.pointInCircle(Main.points[j])) {
                    flag = true;
                    break;
                }
            }
            if (!flag) {
                maybeTri.add(triangleA);
            }
        }
        int index = 0;
        if (!maybeTri.isEmpty()) {
            int rank = 1;
            double len1 = Double.MAX_VALUE;
            double len2 = 0.0;

            for (int ii = 0; ii < maybeTri.size(); ii++) {
                Triangle t = maybeTri.get(ii);
                if (Main.triangleList.contains(t)) {
                    continue;
                }
                if (rank == 1) {
                    if (t.r() >= t.oldEdge.length() / 2) {
                        if (t.r() <= len1) {
                            index = ii;
                        }
                    } else {
                        rank = 2;
                        len2 = t.getS() * 2 / t.oldEdge.length();
                        index = ii;
                    }
                } else {
                    double maybeLen2 = t.getS() * 2 / t.oldEdge.length();
                    if (maybeLen2 >= len2) {
                        len2 = maybeLen2;
                        index = ii;
                    }
                }
            }
            if (!Main.triangleList.contains(maybeTri.get(index))) {
                Main.triangleList.add(maybeTri.get(index));
                Main.stack.push(maybeTri.get(index).newEdges[0]);
                Main.stack.push(maybeTri.get(index).newEdges[1]);
            }
        }
    }

    double length() {
        return Point.getDistanceOf2Points(get(0), get(1));
    }
}
