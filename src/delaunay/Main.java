// Codes by Junxiong Wang
// Main Algorithm by Qi Teng, Xiang Liu

package delaunay;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Stack;

public class Main {
    static ArrayList<Triangle> triangleList = new ArrayList<>();
    static Stack<Edge> stack;
    static Point[] points;
    static int vertices;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String firstLine = scanner.nextLine();

        String[] arg_list = firstLine.split(" ");
        vertices = Integer.parseInt(arg_list[0]);
        /* int dimension = Integer.parseInt(arg_list[1]);
        int attributes = Integer.parseInt(arg_list[2]);
        int boundary = Integer.parseInt(arg_list[3]); */

        points = new Point[vertices];

        for (int i = 0; i < vertices; i++) {
            String new_line = scanner.nextLine();
            String[] new_list = new_line.split(" ");
            double x = Double.parseDouble(new_list[1]);
            double y = Double.parseDouble(new_list[2]);
            points[i] = new Point(x, y, i);
        }

        int initPoint = 0;
        int secondPoint = points[initPoint].findNearestPoint(initPoint, points, vertices);

        Edge initEdge = new Edge(points[initPoint], points[secondPoint]);
        stack = new Stack<>();
        stack.push(initEdge);
        while (!stack.empty()) {
            stack.pop().rock();
        }

        int triangles = triangleList.size();
        int nodesPerTri = 3;

        System.out.printf("%d %d %d\n", triangles, nodesPerTri, 0);
        for (int i = 0; i < triangles; i++) {
            System.out.printf("%d  %d %d %d  0\n", i + 1, points[triangleList.get(i).get(0).index].index + 1,
                    points[triangleList.get(i).get(1).index].index + 1, points[triangleList.get(i).get(2).index].index + 1);
        }
    }
}
