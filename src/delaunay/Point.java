package delaunay;

public class Point {
    double x;
    double y;
    int index;

    Point(double x, double y, int index) {
        this.x = x;
        this.y = y;
        this.index = index;
    }

    int findNearestPoint(int this_index, Point[] points, int vertices) {
        double distance = Double.MAX_VALUE;
        int nextPoint = this_index;
        for (int i = 0; i < vertices; i++) {
            if (i == this_index) {
                continue;
            }
            double newDistance = getDistanceOf2Points(this, points[i]);
            if (newDistance < distance) {
                distance = newDistance;
                nextPoint = i;
            }
        }
        return nextPoint;
    }

    static double getDistanceOf2Points(Point a, Point b) {
        return Math.pow(Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2), 0.5);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Point)) {
            return false;
        }
        return (x == ((Point) obj).x && y == ((Point) obj).y) ||
                (x == ((Point) obj).y && y == ((Point) obj).x);
    }
}
