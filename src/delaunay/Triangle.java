package delaunay;

import java.util.HashSet;

public class Triangle extends Geo {
    Point newPoint;
    Edge oldEdge;
    Edge[] newEdges = new Edge[2];

    Triangle(Point p1, Point p2, Point p3) {
        vertices = new HashSet<>();
        vertices.add(p1);
        vertices.add(p2);
        vertices.add(p3);
    }

    void setNewPoint(Point p) {
        newPoint = p;
    }

    void setEdge(Edge obj, Edge[] newEdges) {
        this.oldEdge = obj;
        this.newEdges = newEdges.clone();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Triangle)) {
            return false;
        }
        return vertices.equals(((Triangle) obj).vertices);
    }

    boolean contains(Point p) {
        return vertices.contains(p);
    }

    Point outerCenter() {
        double x1 = get(0).x;
        double x2 = get(1).x;
        double x3 = get(2).x;
        double y1 = get(0).y;
        double y2 = get(1).y;
        double y3 = get(2).y;

        double x = ((y2 - y1) * (y3 * y3 - y1 * y1 + x3 * x3 - x1 * x1) - (y3 - y1)
                * (y2 * y2 - y1 * y1 + x2 * x2 - x1 * x1))
                / (2 * (x3 - x1) * (y2 - y1) - 2 * ((x2 - x1) * (y3 - y1)));

        double y = ((x2 - x1) * (x3 * x3 - x1 * x1 + y3 * y3 - y1 * y1) - (x3 - x1)
                * (x2 * x2 - x1 * x1 + y2 * y2 - y1 * y1))
                / (2 * (y3 - y1) * (x2 - x1) - 2 * ((y2 - y1) * (x3 - x1)));

        return new Point(x, y, 0);
    }

    double r() {
        Point outerCenter = outerCenter();
        return Point.getDistanceOf2Points(outerCenter, get(0));
}

    boolean pointInCircle(Point p) {
        Point outerCenter = outerCenter();
        double r = Point.getDistanceOf2Points(outerCenter, get(0));
        double d = Point.getDistanceOf2Points(outerCenter, p);
        if (r == d) {
            System.err.println("Multiple points on one circle!!!");
        }
        return d < r;
    }

    int getRank() {
        if (r() > oldEdge.length() / 2) {
            return 3;
        }
        return 0;
    }

    double getS() {
        double a = oldEdge.length();
        double b = newEdges[0].length();
        double c = newEdges[1].length();
        double p = (a + b + c) / 2;
        return Math.pow(p * (p - a) * (p - b) * (p - c), 0.5);
    }
}
